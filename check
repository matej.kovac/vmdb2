#!/bin/bash

set -euo pipefail

echo Running unit tests ============================================
python3 -m CoverageTestRunner --ignore-missing-from=without-tests yarns vmdb
echo

echo Checking every plugin looks OK ============================================
for file in vmdb/plugins/*.py; do
	case "$file" in
	*_plugin.py) ;;
	*)
		echo "File $file should end in _plugin.py" 1>&2
		exit 1
		;;
	esac
	if ! grep -q 'class .*Plugin' "$file"; then
		echo "File $file does not seem to have a Plugin class" 1>&2
		exit 1
	fi
done
echo

if command -v subplot >/dev/null; then
	echo Running Subplot ============================================
	subplot codegen vmdb2.subplot -o test.py
	rm -f test.log
	if ! python3 test.py --log test.log; then
		cat test.log
	fi
	echo
fi

echo Formatting docs ========================================
./format.sh

plugindocs() {
	ls -1 vmdb/plugins/*.mdwn
}

steps() {
	sed -n '/<h2 id="step-.*> Step: /s///p' vmdb2.html
}

code() {
	steps | grep '<code>' || true
}

panic() {
	echo "ERROR: $*" 1>&2
	exit 1
}

for doc in $(plugindocs); do
	n="$(grep -c '^Step:' "$doc")"
	if [ "$n" != 1 ]; then
		panic "Plugin doc $doc must have exactly 1 step title"
	fi
done

n="$(code | wc -l)"
if [ "$n" != 0 ]; then
	code
	panic "Documentation has steps that use code in title"
fi

if ! diff -u <(steps) <(steps | sort); then
	panic "Steps are not in sorted order"
fi
